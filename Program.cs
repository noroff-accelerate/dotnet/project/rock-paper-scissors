﻿using System;

namespace Rock_Paper_Scissors
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare variables
            int playerScore = 0;
            int opponentScore = 0;
            string choice;
            string opponentChoice;
            string playAgainChoice;
            bool validChoice = false;
            bool validPlayAgainChoice = false;
            bool playAgain = false;
            string [] opponentChoices = new string [] {"Rock", "Paper", "Scissors"};
            Random opponentChooser = new Random();

            Console.WriteLine("\t\tWelcome to Rock Paper Scissors");

            //Start game
            do
            {
                do
                {
                    //Prompt for a choice
                    Console.WriteLine("-------------------");
                    Console.WriteLine("Choose");
                    Console.WriteLine("-------------------");
                    Console.WriteLine("Rock");
                    Console.WriteLine("Paper");
                    Console.WriteLine("Scissors");
                    Console.WriteLine();

                    choice = Console.ReadLine();

                    if(choice == "Rock" || choice == "Paper" || choice == "Scissors")
                    {
                        validChoice = true;
                    }
                    else
                    {
                        Console.WriteLine("Please make sure you enter a valid option 'Rock', 'Paper' or 'Scissors'");
                    }
                }//Prompt again until a valid choice is made
                while (validChoice == false);

                //Generates a random number between 0 and 2. This is used to randomly select an item in the opponentChoices array
                opponentChoice = opponentChoices[opponentChooser.Next(0, 2)];

                Console.WriteLine("-------------------");
                Console.WriteLine($"You opponent chose: {opponentChoice}");

                if (choice == "Rock")
                {
                    if(opponentChoice == "Scissors")
                    {
                        Console.WriteLine("You Win! :)");
                        playerScore++;
                    }
                    else if(opponentChoice == "Rock")
                    {
                        Console.WriteLine("Draw :P");
                    }
                    else if(opponentChoice == "Paper")
                    {
                        Console.WriteLine("You Lost! :(");
                        opponentScore++;
                    }
                }

                if (choice == "Paper")
                {
                    if (opponentChoice == "Rock")
                    {
                        Console.WriteLine("You Win! :)");
                        playerScore++;
                    }
                    else if (opponentChoice == "Paper")
                    {
                        Console.WriteLine("Draw :P");
                    }
                    else if (opponentChoice == "Scissors")
                    {
                        Console.WriteLine("You Lost! :(");
                        opponentScore++;
                    }
                }

                if (choice == "Scissors")
                {
                    if (opponentChoice == "Paper")
                    {
                        Console.WriteLine("You Win! :)");
                        playerScore++;
                    }
                    else if (opponentChoice == "Scissors")
                    {
                        Console.WriteLine("Draw :P");
                    }
                    else if (opponentChoice == "Rock")
                    {
                        Console.WriteLine("You Lost! :(");
                        opponentScore++;
                    }
                }

                do
                {
                    Console.WriteLine();
                    Console.WriteLine($"Current Score is: \nYou - \t\t{playerScore} \nOpponent - \t{opponentScore}");
                    Console.WriteLine();
                    Console.WriteLine("Play again?");
                    Console.WriteLine("Choose");
                    Console.WriteLine("Y");
                    Console.WriteLine("N");

                    playAgainChoice = Console.ReadLine();

                    if (playAgainChoice == "Y" || playAgainChoice == "N")
                    {
                        validPlayAgainChoice = true;

                        if (playAgainChoice == "Y")
                        {
                            playAgain = true;
                        }
                        else
                        {
                            playAgain = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please make sure you enter a valid option 'Y' or 'N'");
                    }
                    
                }//Prompt again until a valid choice is made
                while (validPlayAgainChoice == false);

                validChoice = false;

            }//Repeat entire process if Y was selected. Loop again from Start Game
            while (playAgain == true);
        }
    }
}
